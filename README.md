# task0_terminal

Настраиваем терминал
----------------------------
Fish shell

https://gist.github.com/5a96d9808c2c03b8bb28511c8c1580c1.git

https://gist.github.com/a8065236bcfc4caea7fb6256cc93c066.git - ну это почти стандартный конфиг из https://github.com/gpakosz/.tmux

https://gist.github.com/177a43c4f1922bd74efb8c4640040d34.git - алиасы

https://github.com/oh-my-fish/oh-my-fish - фиш взят отсюда. Cо шрифтами пришлось только помучаться при настройке


----------------------------
Vim NerdTree

Nerdtree gives you a little window to navigate a directory structure. I find it helpful in getting a big picture view of how a code project is laid out. To add it, run:

git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree

Next, to have it automatically start up, add the following line to your ~/.vimrc file:

autocmd vimenter * NERDTree

Now, just run vim in any directory and you should get a navigation pane in a split window view.



